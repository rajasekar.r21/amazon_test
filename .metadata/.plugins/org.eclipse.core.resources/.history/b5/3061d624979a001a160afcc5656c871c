package pom.amazon.testCases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.json.Json;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.jayway.jsonpath.JsonPath;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.functions.ExpectedCondition;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import pom.amazon.utilities.ReadConfig;

public class BaseClass {
	ReadConfig config = new ReadConfig();
	
	static AppiumDriver driver;
	String context;
	String locatorType;
	String locatorString;
	public WebDriverWait wait;
	public enum Direction {
		UP, DOWN, LEFT, RIGHT;
	}
	public String OSType = config.getOS();
	public String appPackage = config.getAppPackage();
	public String appActivity = config.getAppActivity();
	JSONParser parser = new JSONParser();
	
	@BeforeTest
	public void setup() throws IOException, ParseException {
		System.out.println("Driver creation");
		
		String obj = parser.parse(new FileReader(System.getProperty("user.dir")+File.separator+"src/test/java"+File.separator+"pom/amazon/testData"+File.separator+"DeviceSheet.json")).toString();
        System.out.println("json object : "+obj);
		
		//Set the Desired Capabilities
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "Realme 2 Pro");
		caps.setCapability("udid", "62d2a850");
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "9.0");
		caps.setCapability("appPackage", appPackage);
		caps.setCapability("appActivity", appActivity);
		caps.setCapability("noReset", "true");
		
		//Instantiate Appium Driver for Android
		try {
				driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
				Reporter.log("Android driver created");
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (MalformedURLException e) {
			    System.out.println(e.getMessage());
		}
		
	}
	
	//Get WebElement
	protected AZWebElement getElement(String[] bothObject){
		String object;
		if(OSType.equalsIgnoreCase("android")) {
			object = bothObject[0];
		}else {
			object = bothObject[1];
		}
		context = object.split(":")[0].split("\\.")[0];
		locatorType = object.split(":")[0].split("\\.")[1];
		locatorString = object.split(":",2)[1].trim();
		
		return getElementLocator(locatorType, locatorString);
	}
	
	//Get WebElement of Dynamic Xpath
	protected AZWebElement getElement(String[] bothObject, int replacement){
		String object;
		if(OSType.equalsIgnoreCase("android")) {
			object = bothObject[0];
		}else {
			object = bothObject[1];
		}
		context = object.split(":")[0].split("\\.")[0];
		locatorType = object.split(":")[0].split("\\.")[1];
		locatorString = object.split(":",2)[1].trim();
		locatorString = locatorString.replaceAll("<<<>>>", Integer.toString(replacement));
		
		return getElementLocator(locatorType, locatorString);
	}
	
	//Get the WebElement
	protected AZWebElement getElementLocator(String locatorType, String locatorString){
		
		By useBy = null;
		switch (locatorType) {
		case "XPATH":
			useBy = By.xpath(locatorString);
			break;
		case "CSS":
			useBy = By.cssSelector(locatorString);
			break;
		case "ID":
			useBy = By.id(locatorString);
			break;
		case "NAME":
			useBy = By.name(locatorString);
			break;
		case "VISUAL":
			useBy = By.linkText(locatorString);
			break;
		default:
			useBy = By.xpath(locatorString);
		}
		
		return new AZWebElement((new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(useBy)));
		
	}
	
	//KeyBoard Event for Enter
	public void keyBoardEvent() {
		AndroidDriver pressKey = (AndroidDriver) driver;
		pressKey.pressKeyCode(AndroidKeyCode.ENTER);
	}
	
	//Close Driver
	@AfterTest
    public void teardown(){
		System.out.println("teardown method");
		try {
			takeScreenShot();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        driver.quit();
    }
	
	
	//Scroll the page With Direction and maximum scroll count
	public void scrollAndSearch(int maxScroll, Direction swipeDirection) {
		for (int i = 0; i <= maxScroll; i++) {
				try {
					swipe(swipeDirection);
				} catch (Exception e2) {
					throw e2;
				}
		}
	}
	
	//Set Start and End Coordinated for each Direction for Swipe
	public void swipe(Direction swipeDirection) {
		String strStartCoordinates = null;
		String strEndCoordinates = null;

		switch (swipeDirection) {
		case DOWN:
			strStartCoordinates = "50%,45%";
			strEndCoordinates = "50%,70%";
			break;
		case LEFT:
			strStartCoordinates = "80%,60%";
			strEndCoordinates = "10%,60%";
			break;
		case RIGHT:
			strStartCoordinates = "30%,50%";
			strEndCoordinates = "80%,50%";
			break;
		case UP:
			strStartCoordinates = "50%,70%";
			strEndCoordinates = "50%,45%";
			break;
		default:
			strStartCoordinates = "50%,70%";
			strEndCoordinates = "50%,45%";
			break;
		}

		swipe(strStartCoordinates, strEndCoordinates);
	}
	
	
	//Perform Swipe Action
	public void swipe(String start, String end) {
		TouchAction touchAction = new TouchAction((MobileDriver) driver);
		Dimension size = driver.manage().window().getSize();

		String[] startTemp = start.split(",");
		String[] endTemp = end.split(",");
		int startx = Integer.parseInt((startTemp[0].replaceAll("%", "").trim()));
		startx = (int) ((size.width * startx) / 100);

		int starty = Integer.parseInt((startTemp[1].replaceAll("%", "").trim()));
		starty = (int) (size.height * starty / 100);

		int endx = Integer.parseInt((endTemp[0].replaceAll("%", "").trim()));
		endx = (int) (size.width * endx / 100);

		int endy = Integer.parseInt((endTemp[1].replaceAll("%", "").trim()));
		endy = (int) (size.height * endy / 100);
		
		touchAction.press(PointOption.point(startx, starty))
		.waitAction(WaitOptions.waitOptions(Duration.ofMillis(0))).moveTo(PointOption.point(endx, endy))
		.release().perform();
	}
	
	//Take Screen shots
	public void takeScreenShot() throws IOException {
		File file  = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file, new File(System.getProperty("user.dir")+File.separator+"Screenshots"+File.separator+"Fail.png"));
	}
}
