package pom.amazon.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;

public class ReadProductDetails {
	
	Properties pro;
	static Object obj;
	
	public ReadProductDetails() {
		String src = System.getProperty("user.dir")+File.separator+"src/test/java"+File.separator+"pom/amazon/testData"+File.separator+"ProductsDetails.json";
		
		try {
			String deviceSheet = new String(Files.readAllBytes(Paths.get(src)));
			obj = Configuration.defaultConfiguration().jsonProvider().parse(deviceSheet);
		}catch(Exception e) {
			System.out.println("Exception while fetching the Product Details sheet "+e);
		}
	}
	
	public String getProductKeyword(String searchkeyWord) {
		return String.valueOf(JsonPath.read(obj, "$.productsKeyword."+searchkeyWord));
	}
	
}
