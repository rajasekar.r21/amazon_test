package pom.amazon.testCases;

import org.testng.Assert;
import org.testng.annotations.Test;

import pom.amazon.pageObjects.Cart;
import pom.amazon.pageObjects.HomePage;
import pom.amazon.pageObjects.ProductDetailsPage;
import pom.amazon.pageObjects.ProductMatrixPage;

public class SearchProdutc_VerifyCart_PlaceOrder extends BaseClass{
	
	@Test
	public void testMethod() {
		String searchKeyWord = "tvSearchTerm";
		
		HomePage homePage = new HomePage();
		ProductMatrixPage pmp = new ProductMatrixPage();
		ProductDetailsPage pdp = new ProductDetailsPage();
		Cart cart = new Cart();
		
		homePage.search(searchKeyWord);
		
		pmp.selectProduct(Integer.toString(2));
		
		String[] productDetails = pdp.getProductDetails();
		pdp.clickAddToCart();
		pdp.navigateToCart();
		
		cart.verifyProductDetails(productDetails);
		cart.clickProceedToCheckout();
	}
}
