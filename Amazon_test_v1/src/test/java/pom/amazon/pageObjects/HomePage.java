package pom.amazon.pageObjects;

import org.testng.Assert;

import pom.amazon.testCases.BaseClass;
import pom.amazon.utilities.ReadProductDetails;

public class HomePage extends BaseClass{
	static String page = "Home Page";
	
	//Xpath are stored as string array
	//First String object for Android and Second String object for iOS
	String[] searchbar = {"NATIVE_APP.XPATH://*[@resource-id='com.amazon.mShop.android.shopping:id/rs_search_src_text']",""} ;
	String[] pmpConfirmation = {"NATIVE_APP.XPATH://*[@resource-id='intermediateRefinements']",""} ;
	
	//Get Product Details
	ReadProductDetails productdetails;
    
	//Search and Navigate to the Product Matrix Page
	public void search(String serachkeyword) {
		productdetails = new ReadProductDetails();
		String searchKeyWord = productdetails.getProductKeyword(serachkeyword);
		
		getElement(searchbar).click(page, "searchbar");
		getElement(searchbar).sendKeys(page, "searchbar",searchKeyWord);
		keyBoardEvent();
		
		Assert.assertTrue(getElement(pmpConfirmation).isDisplayed("PMP", "pmpConfirmation"), "PMP page is not displayed.");
	}
}
