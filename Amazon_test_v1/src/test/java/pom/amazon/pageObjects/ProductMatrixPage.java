package pom.amazon.pageObjects;

import org.testng.Assert;

import pom.amazon.testCases.BaseClass;

public class ProductMatrixPage extends BaseClass{
	static String page = "Prodct Matrix Page";
	
	//Xpath are stored as string array
	//First String object for Android and Second String object for iOS
	String[] PMPTiles = {"NATIVE_APP.XPATH://*[@index='<<<>>>']",""} ;
	String[] PDPTitle = {"NATIVE_APP.XPATH://*[@resource-id='title']",""} ;
    
	//Select a Product based on the Position
	public void selectProduct(String position) {
		int PMPindex = Integer.valueOf(position)*9;
		getElement(PMPTiles, Integer.toString(PMPindex)).click(page, "PMPTiles");
		
		Assert.assertTrue(getElement(PDPTitle).isDisplayed(page, "PDPTitle"), "PDP page is not displayed");
		
	}
	
	//Scroll and Select Random Product
	public void scrollAndSelectProduct() {
		scrollAndSearch(3, Direction.UP);
		getElement(PMPTiles, Integer.toString(2)).click(page, "PMPTiles");

	}

}
